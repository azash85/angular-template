angular.module('mainApp', ['ngRoute']).

constant( 'baseUrl', 'http://api.base.com/' ).


config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider) {
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
    .when('/', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html'
    }) 
    .when('/test', { 
        controller: 'testCtrl', 
        templateUrl: 'src/app/views/test.html' 
    })
    .otherwise({ redirectTo: '/' });
});