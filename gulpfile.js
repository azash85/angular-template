var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')
var rename = require('gulp-rename')
var minifyCss = require('gulp-minify-css')
var sass = require('gulp-sass')

gulp.task('js', function () {
  gulp.src(['src/app/app.js', 'src/app/**/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
})

gulp.task('sass', function () {
  gulp.src('src/scss/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('src/css'))
});

gulp.task('watch', function () {
  gulp.watch('src/app/**/*.js', { interval: 500 }, ['js'])
  gulp.watch('src/scss/**/*.scss', { interval: 500 }, ['sass'])
  gulp.watch('src/css/*.css', { interval: 500 }, ['minify-css'])
  gulp.watch('src/img/**/*', { interval: 500 }, ['copy-img'])
  gulp.watch('src/font/**/*', { interval: 500 }, ['copy-font'])
})

gulp.task('minify-css', function() {
    return gulp.src('src/css/*.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/css'));
})

gulp.task('copy-img', function () {
    return gulp.src(['src/img/**/*'], {
        base: 'src'
    }).pipe(gulp.dest('dist'));
})

gulp.task('copy-font', function () {
    return gulp.src(['src/font/**/*'], {
        base: 'src'
    }).pipe(gulp.dest('dist'));
})

gulp.task('default', ['watch', 'js', 'minify-css', 'copy-img', 'copy-font', 'sass'])